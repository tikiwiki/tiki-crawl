<?php

$options = [
  'timeout'         => 60,          // in seconds
  'cli'             => true,
  'log'             => true,
  'store'           => false,
  'max_size'        => 2,           // in Mb
  'limit'           => 5,           // set to false for unlimited crawl
  'js'              => false,
  'internal'        => true,
  'concurrency'     => 10,
  'max_depth'       => false,
  'delay'           => false,       // number in milliseconds
  'skip_urls'       => false,       // array of specific urls not to parse
  'allow_redirect'  => false,
  'ignore_robots'   => false,       // do not take robots.txt in account
  'allow_nofollow'  => false,       // follow link even if they have a rel="nofollow"
  'user_agent'      => 'TikiCrawl v0.0.2',
];

if (file_exists(__DIR__ . '/config.php')) {
  include __DIR__ . '/config.php';
}
