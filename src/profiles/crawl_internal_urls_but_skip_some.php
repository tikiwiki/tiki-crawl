<?php

namespace TikiCrawl\Profiles;

use GuzzleHttp\Psr7\Uri;
use Psr\Http\Message\UriInterface;
use Spatie\Crawler\CrawlProfiles\CrawlProfile;

class CrawlInternalUrlsButSkipSome extends CrawlProfile
{
  protected mixed $baseUrl;
  protected array $skipList;

  public function __construct($baseUrl, $skipList)
  {
    if (! $baseUrl instanceof UriInterface) {
      $baseUrl = new Uri($baseUrl);
    }

    $this->baseUrl = $baseUrl;
    $this->skipList = $skipList;
  }

  public function shouldCrawl(UriInterface $url): bool
  {
    return (
      $this->baseUrl->getHost() === $url->getHost() &&
      !in_array($url->__toString(), $this->skipList)
    );
  }
}
