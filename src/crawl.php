<?php

namespace TikiCrawl;

require dirname(__DIR__) . '/vendor/autoload.php';
require dirname(__DIR__) . '/src/observers/cli_dot.php';
require dirname(__DIR__) . '/src/observers/logger.php';
require dirname(__DIR__) . '/src/observers/store_to_files.php';
require dirname(__DIR__) . '/src/profiles/crawl_internal_urls_but_skip_some.php';
require dirname(__DIR__) . '/src/utils/store.php';
require dirname(__DIR__) . '/src/utils/log.php';


use Spatie\Crawler\Crawler;
use Spatie\Crawler\CrawlProfiles\CrawlInternalUrls;
use GuzzleHttp\Psr7\Uri;
use GuzzleHttp\RequestOptions;
use TikiCrawl\Observers\Logger;
use TikiCrawl\Observers\CliDot;
use TikiCrawl\Observers\StoreToFiles;
use TikiCrawl\Profiles\CrawlInternalUrlsButSkipSome;
use TikiCrawl\Utils\Log;

class Crawl {
  private Crawler $crawler;
  public Uri $url;

  function __construct(String $url, Array $options)
  {
    $crawler_options = array(
      RequestOptions::CONNECT_TIMEOUT => $options['timeout'],
      RequestOptions::TIMEOUT => $options['timeout'],
      RequestOptions::ALLOW_REDIRECTS => $options['allow_redirect'],
      RequestOptions::HEADERS => [
        'User-Agent' => $options['user_agent']
      ]
    );
    $this->crawler = Crawler::create($crawler_options);
    $this->url = new Uri($url);
    if ($options['cli']) {
      $this->crawler->addCrawlObserver(new CliDot());
    }
    if ($options['log']) {
      $this->crawler->addCrawlObserver(new Logger());
    }
    if ($options['store']) {
      $this->crawler->addCrawlObserver(new StoreToFiles($this->url));
    }
    if ($options['internal']) {
      $this->crawler->setCrawlProfile(new CrawlInternalUrls($this->url));
    }
    if ($options['js']) {
      $this->crawler->executeJavaScript();
    }
    if ($options['internal']) {
      if ($options['skip_urls']) {
        $this->crawler->setCrawlProfile(new CrawlInternalUrlsButSkipSome($this->url, $options['skip_urls']));
      } else {
        $this->crawler->setCrawlProfile(new CrawlInternalUrls($this->url));
      }
    }
    if ($options['limit']) {
      $this->crawler->setTotalCrawlLimit($options['limit']);
    }
    if ($options['concurrency']) {
      $this->crawler->setConcurrency($options['concurrency']);
    }
    if ($options['max_depth']) {
      $this->crawler->setMaximumDepth($options['max_depth']);
    }
    if ($options['delay']) {
      $this->crawler->setDelayBetweenRequests($options['delay']);
    }
    if ($options['ignore_robots']) {
      $this->crawler->ignoreRobots();
    }
    if ($options['allow_nofollow']) {
      $this->crawler->acceptNofollowLinks();
    }
    $this->crawler->setMaximumResponseSize(1024 * 1024 * $options['max_size']);
    $this->crawler->setParseableMimeTypes(['text/html', 'text/plain']);
    $this->crawler->setDefaultScheme('https');
    Log::add("-------------------\nStarting crawl $url\n\n");
  }

  public function run(): void
  {
    $this->crawler->startCrawling($this->url);
    Log::add("\nCrawl finished\n");
  }
}