<?php

namespace TikiCrawl\Utils;

class Log
{
  protected const access_path = __DIR__ . '/../../logs/access.log';
  protected const error_path = __DIR__ . '/../../logs/error.log';

  public static function add(string $text): void
  {
    $text = Log::timestamp() . ' - ' . $text;
    file_put_contents(static::access_path, $text, FILE_APPEND);
  }
  public static function err(string $text): void
  {
    $text = Log::timestamp() . ' - ' . $text;
    file_put_contents(static::error_path, $text, FILE_APPEND);
  }

  public static function reset(): void
  {
    file_put_contents(static::access_path, 'start log' . PHP_EOL);
    file_put_contents(static::error_path, 'start log' . PHP_EOL);
  }

  private static function timestamp(): string
  {
    return date_create()->format('Y-m-d H:i:s');
  }
}