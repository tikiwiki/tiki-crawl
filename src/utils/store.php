<?php

namespace TikiCrawl\Utils;

use Psr\Http\Message\UriInterface;

class Store
{
  public $basepath;
  public $path;

  function __construct(UriInterface $url)
  {
    $rootpath = realpath(dirname(dirname(__DIR__)) . '/collected-data/');
    $this->basepath = $rootpath . '/' . $url->getHost();
    if (!is_dir($this->basepath)) {
      mkdir($this->basepath, 0755);
    }
  }

  public function save(UriInterface $url, string $text, string $extension): void
  {
    if (substr($url->getPath(), -1, 1) == '/') {
      $path = substr($url->getPath(), 1, -1);
      $filename = 'index.html';
    } else {
      $path = substr(dirname($url->getPath()), 1);
      $filename = basename($url->getPath());
    }
    if ($url->getQuery() != '') {
      $path .= $filename . '-q';
      $filename = $this->clean_query($url->getQuery());
    }
    if ($extension != '') {
      $filename = preg_replace('/\.'.$extension.'$/', '', $filename) . '.' . $extension;
    }
    if (!is_dir($this->basepath.'/'.$path)) {
      mkdir($this->basepath.'/'.$path, 0755, true);
    }
    $this->path = $path.'/'.$filename;
    file_put_contents($this->basepath.'/'.$this->path, $text);
  }

  private function clean_query(string $query): string
  {
    // this may need some sanitization
    $query = strtr($query, '/', '.');
    $query = str_replace('=', '__', $query);
    return $query;
  }
}