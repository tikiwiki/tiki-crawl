<?php

namespace TikiCrawl\Observers;

use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UriInterface;
use Spatie\Crawler\CrawlObservers\CrawlObserver;
use TikiCrawl\Utils\Log;

class Logger extends CrawlObserver
{

  function willCrawl(UriInterface $url): void
  {
    Log::add("Preparing $url \n");
  }

  function crawled(
        UriInterface $url,
        ResponseInterface $response,
        ?UriInterface $foundOnUrl = null
    ): void
    {
      Log::add("Crawling $url - ".
        $response->getHeader('content-type')[0].
        " - from $foundOnUrl\n");
    }

    function crawlFailed(
        UriInterface $url,
        RequestException $requestException,
        ?UriInterface $foundOnUrl = null
    ): void
    {
      Log::err("Failed $url (from $foundOnUrl) : '. $requestException .'\n");
    }

    function finishedCrawling(): void
    {
      Log::add("All done\n\n");
    }

}
