<?php

namespace TikiCrawl\Observers;

use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UriInterface;
use Spatie\Crawler\CrawlObservers\CrawlObserver;
use TikiCrawl\Utils\Store;

class StoreToFiles extends CrawlObserver
{
  public $store;
  public $report_path;
  public $errors_path;

  // check http://svn.apache.org/repos/asf/httpd/httpd/trunk/docs/conf/mime.types
  // for a full list.
  // only list here types for which we will enforce an extension for saved files
  private $mime_types = array(
    'application/msword' => 'doc',
    'application/pdf' => 'pdf',
    'image/jpeg' => 'jpg',
    'image/png' => 'png',
    'text/html' => 'html',
    'video/mp4' => 'mp4',
    'audio/mpeg' => 'mp3',
    'text/csv' => 'csv',
    'application/epub+zip' => 'epub',
    'application/rss+xml' => 'xml',
    'application/zip' => 'zip',
    'application/vnd.ms-excel' => 'xls',
    'application/vnd.ms-excel.sheet.macroEnabled.12' => 'xls',
    'application/vnd.ms-powerpoint' => 'ppt',
    'application/vnd.openxmlformats-officedocument.presentationml.presentation' => 'pptx',
    'application/vnd.openxmlformats-officedocument.presentationml.slideshow' => 'ppsx',
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' => 'xlsx',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => 'docx'
  );

  function __construct(UriInterface $rooturl)
  {
    $this->store = new Store($rooturl);
    $this->report_path = $this->store->basepath . '-stored.tsv';
    $this->errors_path = $this->store->basepath . '-failed.tsv';
  }

  function willCrawl(UriInterface $url): void
  {
  }

  function crawled(
        UriInterface $url,
        ResponseInterface $response,
        ?UriInterface $foundOnUrl = null
    ): void
    {
      $this->store->save(
        $url,
        $response->getBody(),
        $this->getExtension($response)
      );
      file_put_contents($this->report_path, 
        sprintf("%s\t%s\t%s\n",
          $foundOnUrl?->__toString(),
          $url,
          $this->store->path
        ),
        FILE_APPEND
      );
    }

    function crawlFailed(
        UriInterface $url,
        RequestException $requestException,
        ?UriInterface $foundOnUrl = null
    ): void
    {
      file_put_contents($this->errors_path, 
        sprintf("%s\t%s\t%s\t%s\n",
          $foundOnUrl?->__toString(),
          $url,
          $this->store->path,
          $requestException->getMessage()
        ),
        FILE_APPEND
      );
    }

    private function getExtension(ResponseInterface $response): string
    {
      $type = $response->getHeader('content-type')[0];
      if (strpos($type, ';')) {
        $type = substr($type, 0, strpos($type, ';'));
        $type = str_replace(';', '', $type);
      }
      if (array_key_exists($type, $this->mime_types)) {
        return $this->mime_types[$type];
      }
      return '';
    }
}
